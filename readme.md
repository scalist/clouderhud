# ClouderHud
Welcome to the ClouderHud readme. This repository contains the source files for ClouderHud. Feel free to add pull requests, issues, etc..

ClouderHud is an open-source Hud for Garry's Mod. It can be used on Sandbox or DarkRP game modes at this time. ClouderHud is designed to be sleek but very easy to edit. In the source files of ClouderHud, we have given a brief explanation of what each line does and it's purpose. It is easy to install, coming with a easy-to-use GUI installer to get you up & running in no time!

<b>This repository is maintained by http://scalist.net.</b>

## Installation (clientside)
If you are just using this HUD on a non-dedicated DarkRP server, follow these steps
1. Clone/Download this repository onto your PC
2. Open up the Garry's Mod directory, on Windows it is: ```C:\Program Files (x86)\Steam\steamapps\common\GarrysMod\garrysmod\lua\autorun\client```
3. Copy the ```cl_clouderhud_x.lua``` file into the directory listed above

## Installation (serverside)
If you are a server host on Garry's Mod & would like to use this HUD in your sever, follow these steps
1. Initalize a dedicated server running the DarkRP gamemode & the DarkRP edit addon
2. Clone/Download this repository onto your Server. For a Linux server, use: ```sudo apt-get install git``` ```mkdir ~/clouderhud``` ```cd ~/clouderhud``` ```sudo git clone https://github.com/Scalist/ClouderHud.git```
3. Head to your created server directory & copy the address
4. In Linux, use the ```mv``` command to move the ```cl_clouderhud_x.lua``` to the installation directory.

## Notes & Disclaimers
- There <b>will always</b> be bugs! If you find any, always remember to report them on the "issues" tab. If you think you can fix a bug, it is always welcome to submit a pull request.
- If you are using this <b>Commercially</b>, please credit us (Scalist.net) somewhere in your server that players can see (not in an inaccessible area). This is not a rule, just a recommendation.
