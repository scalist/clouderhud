-- ClouderHud // Developed by:
-- ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
--▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
--▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░▌           ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ 
--▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌               ▐░▌     ▐░▌               ▐░▌     
--▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░▌               ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄      ▐░▌     
--▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░▌               ▐░▌     ▐░░░░░░░░░░░▌     ▐░▌
-- ▀▀▀▀▀▀▀▀▀█░▌▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░▌               ▐░▌      ▀▀▀▀▀▀▀▀▀█░▌     ▐░▌          
--          ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌               ▐░▌               ▐░▌     ▐░▌     
-- ▄▄▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄█░█▄▄▄▄  ▄▄▄▄▄▄▄▄▄█░▌     ▐░▌     
--▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     
-- ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀      

-- Config
allow_user_interation = true -- allows the user to interact with the HUD using console commands. For server developers
showhud_death = true -- Shows the death HUD
hidehud = false -- set this to true if you don't want anything to show

-- Loading HUD starting message
print("(HUD) Main HUD has started loading!")

-- Custom Fonts
surface.CreateFont( "Font24", { -- Font 1
	font = "Bebas",
	size = 24,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "Font30", { -- Font 2
	font = "Bebas",
	size = 30,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "Font50", { -- Font 3
	font = "Bebas",
	size = 50,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "FontDS", { -- Font 4
	font = "Bebas",
	size = 100,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "FontDS2", { -- Font 5
	font = "Bebas",
	size = 33,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

-- Hide the HUD command
if allow_user_interation == true then -- if user interaction is enabled then
	-- create hide hud concommand
	concommand.Add("clh_hud_hide", function(ply, args)
		print("(HUD) clh_hud_hide command activated! Hiding client's HUD!")
		hidehud = true
	end)
end

-- Show the HUD command
if allow_user_interation == true then -- if user interaction is enabled then
	-- create show hud concommand
	concommand.Add("clh_hud_show", function(ply, args)
		print("(HUD) clh_hud_show command activated! Showing client's HUD!")
		hidehud = false
	end)
end

-- Disable default HUD & red overlay on death
hook.Add( "HUDShouldDraw", "clh_hidehud_default", function(name)
	if (name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudCrosshair" or name == "CHudDamageIndicator") then
		return false
	end
end)

-- Draws the HUD
hook.Add("HUDPaint", "DrawMyHud", function() -- HUD hook

	if (deathhidehud == false and hidehud == false) then

		draw.RoundedBox(10,20,ScrH()-150,300,95,Color(50,50,50,225)) -- Background box higher
		draw.RoundedBox(10,20,ScrH()-65,300,45,Color(35,35,35,255)) -- Background box lower
		
		surface.SetDrawColor(35,35,35,255) -- Set colour for divider
		surface.DrawRect(20,ScrH()-65,300,7) -- Background box divider
		
		local health = LocalPlayer():Health() -- Creates a variable to shorten health
		
		-- Health overflow fixer
		if (health >= 101) then
			health = 100
		end
		
		-- Back to HUD
		draw.RoundedBox(5,25,ScrH()-60,health*2.9,15,Color(200,0,0)) -- Health bar
		
		local armor = LocalPlayer():Armor() -- Creates a variable to shorten armour

		-- Armor overflow fixer
		if (armor >= 101) then
			armor = 100
		end
		
		-- Armor underflow fixer
		if armor == nil then
			armor = 0 -- Nothing happens when you change this, just a placeholder value
		end
		
		-- Back to HUD // armor*3-10,15
		draw.RoundedBox(5,25,ScrH()-40,armor*3-10,15,Color(0,157,255,255)) -- Armour bar 

		draw.DrawText(LocalPlayer():Nick(),"Font30",105,ScrH()-146,Color(255,255,255)) -- Player's name

		-- Ammo HUD
		draw.RoundedBox(10,ScrW()-320,ScrH()-115,300,60,Color(50,50,50,225)) -- Background box higher
		draw.RoundedBox(10,ScrW()-320,ScrH()-65,300,45,Color(35,35,35,255)) -- Background box lower
			
		surface.SetDrawColor(35,35,35,255) -- Set colour for divider
		surface.DrawRect(ScrW()-320,ScrH()-65,300,7) -- Background box divider

		-- If player is alive then
		if (LocalPlayer():GetActiveWeapon():IsValid()) then

			-- Finds & displays client's current weapon ammo
			if (LocalPlayer():GetActiveWeapon():Clip1() != -1) then
				draw.SimpleText("Ammo: " .. LocalPlayer():GetActiveWeapon():Clip1() .. "/" .. LocalPlayer():GetAmmoCount(LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType()),"Font50",ScrW()-315,ScrH()-115,Color(255,255,255),0,0) -- "Ammo" text
			else
				draw.SimpleText("Ammo:" .. LocalPlayer():GetAmmoCount(LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType()),"Font50",ScrW()-315,ScrH()-115,Color(255,255,255),0,0)
			end

			-- Finds & displays client's current weapon
			if (LocalPlayer():GetActiveWeapon():GetPrintName() != nil) then
				draw.SimpleText("Gun: " .. LocalPlayer():GetActiveWeapon():GetPrintName(),"Font30",ScrW()-315,ScrH()-60,Color(255,255,255),0,0)
			end
		end
	end

	if (LocalPlayer():GetActiveWeapon():IsValid() == false and showhud_death == true) then -- If player is dead & death hud has been enabled in the config, then..

		-- Hide HUD so doesnt get in way of Deathscreen
		deathhidehud = true

		-- Make variables to center screen
		local center_w = ScrW() / 2
		local center_h = ScrH() / 2

		-- Death HUD
		draw.RoundedBox(10,center_w-175,center_h-58,300,88,Color(50,50,50,225)) -- Background box higher
		draw.RoundedBox(10,center_w-175,center_h+25,300,45,Color(35,35,35,255)) -- Background box lower

		surface.SetDrawColor(35,35,35,255) -- Set colour for divider
		surface.DrawRect(center_w-175,center_h+24,300,7) -- Background box divider

		draw.SimpleText("You died!","FontDS",center_w-165,center_h-70,Color(255,255,255,225)) -- Main text
		draw.SimpleText("press <space> to  respawn","FontDS2",center_w-170,center_h+28,Color(255,255,255,225)) -- Respawn text and instructions
	else
		deathhidehud = false
	end
end)

-- Make a hook that scans for when LocalPlayer() is active
hook.Add("InitPostEntity", "LocalPlayerCreate", function(pl) -- first Entity is LocalPlayer
	
	hook.Remove("InitPostEntity","LocalPlayerCreate") -- Call this hook once

	-- Display avatar (here LocalPlayer() return is valid player)
	local Avatar = vgui.Create("AvatarImage")
		Avatar:SetSize(72,72)
		Avatar:SetPos(27.5,ScrH()-142)
		Avatar:SetPlayer(LocalPlayer(),64)
end)

-- Loading HUD completed message
print("(HUD) Main HUD completed loading!")
